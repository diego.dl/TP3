// Port to listen requests from
var port = 1234;

const cookie_name = "token";

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var Hashes = require('jshashes');
var cookieParser = require('cookie-parser');
var path = require('path');

var SHA256 =  new Hashes.SHA256;
var SHA512 = new Hashes.SHA512;

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Use cookie parser middleware
app.use(cookieParser());

// Serve static files
app.use('/login', express.static(path.join(__dirname, 'public')));

app.use('/static', express.static(path.join(__dirname, 'public')));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

//Log sessions
app.get("/sessions", (req, res, next) => {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});

// Disconnect
app.post("/disconnect", (req, res, next) => {
	let cookie = req.cookies[cookie_name];
	if(cookie) {
		res.clearCookie(cookie_name);
	}
	res.redirect('/');
});

app.post("/login", function(req, res, next) {
	let login = req.body.login,
		passwd = req.body.passwd,
		sql = "SELECT * FROM users WHERE ident = ? AND password = ?";
	db.all(sql, [login, passwd], (err, rows) =>{
		if(err){
			console.log(err);
			res.status(500).send({status: false, err});
		} else if(rows.length > 0){
			// Token with Hashes
			let token =  SHA512.hex( SHA512.hex(req.body.login) + SHA512.hex(req.body.password));
			
			// Run asynchronously because client doesn't have to wait for the result
			sql = "INSERT INTO sessions Values(?, ?)";
			db.run(sql, [login, token]);
			// Send cookie
			res.cookie(cookie_name, token);
			
			res.redirect('/');
		} else {
			res.status(404).send({status: false});
		}
	});
});

app.get("/", (req, res, next) => {
	let token = req.cookies[cookie_name];
	if(token){
		let sql = "SELECT * FROM sessions WHERE token = ?";
		db.all(sql, [token], (err, rows) => {
			if(err) {
				console.log(err);
				res.status(500).send({status: false, err});
			} else if(rows.length > 0){
				res.status(200).sendFile(path.join(__dirname , 'public', 'connected.html'));
			} else {
				res.status(400).send({status: false, err: "token invalid"});
			}
		});
	} else {
		res.redirect('/login');
	}
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
